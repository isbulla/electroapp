import 'package:flutter/material.dart';
import 'rooms.dart';
import 'signup.dart';

class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return _LoginPage();
  }
}

class _LoginPage extends State<LoginPage>{
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.red,
        radius: 48.0,
        //child: Image.asset('assets/logo.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: '',
      decoration: InputDecoration(
        hintText: 'Email',
       // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
      ),
      //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: '',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0), 
      child:MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).push(
               new MaterialPageRoute(builder: (context){
                  return new RoomsPage();
            }));
          },
          color: Theme.of(context).buttonColor,
          child: Text('LOGIN', style: TextStyle(color: Colors.white)),
        ),
      );

    final noAccountLabel = FlatButton(
      child: Text(
        'No account yet? Create one',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
       Navigator.of(context).push(
               new MaterialPageRoute(builder: (context){
                  return new SignupPage();
            }));
      },
    );

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 46.0),
            password,
            SizedBox(height: 36.0),
            loginButton,
            noAccountLabel
          ],
        ),
      ),
    );
  }
}
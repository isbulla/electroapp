import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'login.dart';
import 'signup.dart';
import 'rooms.dart';
import 'switch.dart';
import 'switchwebview.dart';
String url = "https://youtube.com/";
void main() => runApp(new Login());

class Login extends StatefulWidget{
  @override
  State<StatefulWidget> createState(){
    return _Login();
  }
}

class _Login extends State<Login>{
  final Color prColor=Color.fromRGBO(136, 188, 216, 1.0);
  final Color prBtnColor=Color.fromRGBO(47, 72, 88, 1.0);
  final Color prBtnTextColor=Color.fromRGBO(255, 255, 255, 1.0);
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
      theme: ThemeData(primaryColor: prColor, primaryColorLight:prBtnTextColor,buttonColor: prBtnColor ),
      routes: <String,WidgetBuilder>{
        "/SignupPage":(BuildContext context) => new SignupPage(),
        "/LoginPage":(BuildContext context) => new LoginPage(),
        "/RoomsPage":(BuildContext context) => new RoomsPage(),
        "/SwitchPage":(BuildContext context) => new SwitchPage(),
        "/Switchwebview":(BuildContext context) => new Switchwebview()
      },
    );
  }
}
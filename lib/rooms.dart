import 'dart:async';
import 'package:flutter/material.dart';
import 'switch.dart';
class RoomsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RoomsPage();
  }
}

class _RoomsPage extends State<RoomsPage> {
  List<String> list = new List<String>();
  final formKey = GlobalKey<FormState>();
  String _roomName="";
  void addToList(String str) {
    setState(() {
      list.add(str);
    });
  }
  void _submit(){
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      addToList(_roomName);

    }
  }

  final roomAdd = TextFormField(
    keyboardType: TextInputType.text,
    autofocus: false,
    initialValue: '',
    decoration: InputDecoration(
      hintText: 'Name',
      // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
    ),
    //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
  );

  final addButton = MaterialButton(
    height: 42.0,
    onPressed: () {},
    color: Colors.blue,
    child: Text('LOGIN', style: TextStyle(color: Colors.black)),
  );

  Future<Null> _askedToLead() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new SimpleDialog(
            title: const Text('Add a Room'),
            children: <Widget>[ 
              new Form(child: Column(children: <Widget>[
                new SimpleDialogOption(
                onPressed: () {},
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  initialValue: '',
                  decoration: InputDecoration(
                    hintText: 'Name',
                    // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                  ), onSaved: (input)=>_roomName=input,
                  //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                ),
              ),
                new SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: SizedBox(
                          width: 5.0,
                          height: 5.0,
                        ),
                      ),
                      new Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            _submit();
                            print(_roomName);
                            Navigator.pop(context);
                          },
                          child: Text("Ok"),
                          color: Theme.of(context).buttonColor,
                        ),
                      ),
                      new SizedBox(
                        width: 5.0,
                        height: 5.0,
                      ),
                      new Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            print('Cancel');
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                          color: Theme.of(context).buttonColor,
                        ),
                      ),
                    ],
                  )),
              ],) ,key: formKey,),
              
            ],
          );
        });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Rooms'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.add),
            onPressed: () {
              _askedToLead();
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(list[index]),
                  onTap: () {
                    print(list[index]);
                     Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context){
                          return new SwitchPage(room: list[index]);
                    }));
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

import 'dart:async';
import 'package:flutter/material.dart';
import 'switchwebview.dart';

class SwitchPage extends StatefulWidget {
  final String room;
  SwitchPage({this.room});
  @override
  State<StatefulWidget> createState() {
    return _SwitchPage();
  }
}

class _SwitchPage extends State<SwitchPage> {
  List<String> list = new List<String>();
  final formKey = GlobalKey<FormState>();
  String _sName="";
  String _ipAdd=""; 
  @override
  void initState()  {
    super.initState();
        setState(() {
         list.add('Test');
        });
  }
 
  void _submit(){
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      //addToList(_roomName);
       
    }
  }

  final roomAdd = TextFormField(
    keyboardType: TextInputType.text,
    autofocus: false,
    initialValue: '',
    decoration: InputDecoration(
      hintText: 'Name',
      // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
    ),
    //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
  );

  final addButton = MaterialButton(
    height: 42.0,
    onPressed: () {},
    color: Colors.blue,
    child: Text('LOGIN', style: TextStyle(color: Colors.black)),
  );

  Future<Null> _askedToLead() async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new SimpleDialog(
            title: const Text('Switch Detail'),
            children: <Widget>[ 
              new Form(child: Column(children: <Widget>[
                new SimpleDialogOption(
                onPressed: () {},
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  initialValue: '',
                  decoration: InputDecoration(
                    hintText: 'Name',
                    // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                  ), onSaved: (input)=>_sName=input,
                  //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                ),
              ),
              new SimpleDialogOption(
                onPressed: () {},
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  initialValue: '',
                  decoration: InputDecoration(
                    hintText: 'Ip',
                    // hintStyle: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                  ), onSaved: (input)=>_ipAdd=input,
                  //style: TextStyle(color: Theme.of(context).primaryColorLight,fontSize: 16.0),
                ),
              ),
                new SimpleDialogOption(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: SizedBox(
                          width: 5.0,
                          height: 5.0,
                        ),
                      ),
                      new Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            _submit();
                            print(_sName);
                            //Navigator.pop(context);
                             Navigator.of(context).pushNamed("/Switchwebview");
                          },
                          child: Text("Ok"),
                          color: Theme.of(context).buttonColor,
                        ),
                      ),
                      new SizedBox(
                        width: 5.0,
                        height: 5.0,
                      ),
                      new Expanded(
                        child: MaterialButton(
                          onPressed: () {
                            print('Cancel');
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                          color: Theme.of(context).buttonColor,
                        ),
                      ),
                    ],
                  )),
              ],) ,key: formKey,),
              
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var btns = ["Switch1","Switch2","Switch2","Switch2","Switch2","Switch2","Switch2"];
    var myGridView = new GridView.builder(
      itemCount: btns.length,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (BuildContext context, int index) {
        return new Padding(
          padding: EdgeInsets.all(5.0),
          child: new FlatButton(child: Text(btns[index]),color: Colors.green, onPressed: () {_askedToLead();},), 
        );
      },
    );

    return new Scaffold(
      appBar: new AppBar(
          title: new Text(widget.room)
      ),
      body: myGridView,
    );
  }
 void genButton(){
   
 }
}

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

String url = "https://youtube.com/";

class Switchwebview extends StatefulWidget {
  //final String room;
  //Switchwebview({this.room});
  @override
  State<StatefulWidget> createState() {
    return _Switchwebview();
  }
}

class _Switchwebview extends State<Switchwebview> {
 
  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
      appBar: new AppBar(
          title: new Text("Web"),
      ),
      withJavascript: true,
      withLocalStorage: true,
      withZoom: true,
      url: url,
    );
  }
}
